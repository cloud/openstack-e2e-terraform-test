#!/bin/bash
set -e
THIS_SCRIPT_DIR="$(dirname $(realpath "$0"))"

DOCKER_IMAGE="cerit.io/galaxy/terraform_ostack:latest"

echo "Building $DOCKER_IMAGE ..."
docker build -t $DOCKER_IMAGE $THIS_SCRIPT_DIR

echo "Pushing $DOCKER_IMAGE ..."
docker push $DOCKER_IMAGE
