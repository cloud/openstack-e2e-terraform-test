# OpenStack Infrastructure e2e test

## Usage
* source application credentials
* run:
  * pre-check: `tests/support/scripts/assert_no_resources_in_openstack_project.sh`
  * suite api_compute_and_storage: `tests/suites/api_compute_and_storage/run_api_compute_and_storage.sh`
  * suite api_network: `tests/suites/api_network/run_api_network.sh`
