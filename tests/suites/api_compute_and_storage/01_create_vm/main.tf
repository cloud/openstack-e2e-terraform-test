# ---------------------------------------------------------------------------------------
# for project 86beb553f44549469d686225488a053c (meta-cloud-synthetic-tests, g2-brno-prod)
# ---------------------------------------------------------------------------------------

variable "project_specific__subnet_name" {}
variable "project_specific__project_network_id" {}
variable "project_specific__security_group_ssh_id" {}
variable "project_specific__public_external_network_name" {}

variable "test_specific__infra_name" {}
variable "test_specific__node_flavor_name" {}
variable "test_specific__environment" {}
variable "test_specific__default_ssh_user" {}
variable "test_specific__enable_public_fip" {}

module "vm_1port_2vols" {
  source = "../../../support/terraform/modules/vm_1port_2vols"
  
  subnet_name = var.project_specific__subnet_name
  project_network_id = var.project_specific__project_network_id
  security_group_ssh_id = var.project_specific__security_group_ssh_id
  public_external_network_name = var.project_specific__public_external_network_name

  infra_name = var.test_specific__infra_name
  node_flavor_name = var.test_specific__node_flavor_name
  environment = var.test_specific__environment
  default_ssh_user = var.test_specific__default_ssh_user
  enable_public_fip = var.test_specific__enable_public_fip
  description = "This resource is created as part of automated end-to-end tests"
}

output "cloud_init_rendered"  {
  value = module.vm_1port_2vols.cloud_init_rendered
}

output "new_volume_ids" {
  value = module.vm_1port_2vols.all_data_volume_ids
}
