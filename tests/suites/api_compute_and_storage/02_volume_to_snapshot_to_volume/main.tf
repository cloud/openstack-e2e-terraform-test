variable "test_specific__infra_name" {
  type = string
}

resource "openstack_blockstorage_volume_v3" "source_volume" {
  name = "${var.test_specific__infra_name}--source_volume"
  size = 1
  description = "This resource is created as part of automated end-to-end tests"
}


resource "null_resource" "snapshot" {
  provisioner "local-exec" {
    command = "/bin/bash create_snapshot.sh ${openstack_blockstorage_volume_v3.source_volume.id} snapshot_detail.json ${var.test_specific__infra_name}"
    
  }

  provisioner "local-exec" {
    when    = destroy
    command = "/bin/bash delete_snapshot.sh snapshot_detail.json"
  }

  depends_on = [ openstack_blockstorage_volume_v3.source_volume ]
}

data "local_file" "snapshot_detail" {
  filename = "snapshot_detail.json"
  depends_on = [ null_resource.snapshot ]  # Ensure that this runs after the command
}

locals {
  snapshot_details = jsondecode(data.local_file.snapshot_detail.content)
  snapshot_id    = local.snapshot_details["id"]
}


resource "openstack_blockstorage_volume_v3" "volume_from_snapshot" {
  name = "${var.test_specific__infra_name}--test_snapshot"
  size = 1
  snapshot_id = local.snapshot_id
  description = "This resource is created as part of automated end-to-end tests"
  depends_on = [ null_resource.snapshot ]
}
