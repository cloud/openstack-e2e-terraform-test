#!/bin/bash
existing_source_volume_id=$1
new_snapshot_detail_filename=$2
snapshot_name_prefix=$3

function is_snapshot_in_expected_status() {
  local SNAPSHOT_ID="$1"
  local EXPECTED_SNAPSHOT_STATUS="$2"
  SNAPSHOT_STATUS=$(openstack volume snapshot show $SNAPSHOT_ID -f value -c status)
  if [[ $SNAPSHOT_STATUS == $EXPECTED_SNAPSHOT_STATUS ]]; then
        return 0
  fi
  return 1
}

function wait_till_expected_status() {
  local SNAPSHOT_ID="$1"
  local EXPECTED_SNAPSHOT_STATUS="$2"
  local ATTEMPT_SLEEP_TIME=10
  local ATTEMPT=1
  while true; do  
    ATTEMPT=$((ATTEMPT+1))           
    if is_snapshot_in_expected_status $SNAPSHOT_ID $EXPECTED_SNAPSHOT_STATUS ; then
      return
    fi
        
    if [[ $ATTEMPT -gt 10 ]]; then
        echo "Snapshot $SNAPSHOT_ID failed to become status $EXPECTED_SNAPSHOT_STATUS"      
        return 1
    fi  

    sleep $ATTEMPT_SLEEP_TIME  
  done
}


VOL_NAME=$(openstack volume show $existing_source_volume_id -f json | jq -r '.name')
VOL_NAME="${snapshot_name_prefix}--${VOL_NAME}"
SNAPSHOT_JSON=$(openstack volume snapshot create --volume ${existing_source_volume_id} ${VOL_NAME} -f json)
SNAPSHOT_ID=$(echo "$SNAPSHOT_JSON" | jq -r '.id')
echo $SNAPSHOT_JSON > ${new_snapshot_detail_filename}

wait_till_expected_status $SNAPSHOT_ID "available"
openstack volume snapshot show $SNAPSHOT_ID -f json > ${new_snapshot_detail_filename}  
