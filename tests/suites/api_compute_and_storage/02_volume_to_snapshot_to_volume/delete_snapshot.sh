#!/bin/bash
new_snapshot_detail_filename=$1
snapshot_id=$(jq -r '.id' $new_snapshot_detail_filename)
openstack volume snapshot delete $snapshot_id
# following solve a synchronization problem when deleting the source volume as the snapshot deletion was not really commited in ostack db
# TODO: udelat lip: jet v cyklu a
# TODO START
openstack volume snapshot show  $snapshot_id || true > /dev/null 
sleep 30
# TODO END
