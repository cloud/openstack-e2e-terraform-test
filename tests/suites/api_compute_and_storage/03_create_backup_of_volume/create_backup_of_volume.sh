#!/bin/bash
existing_volume_id=$1
new_backup_detail_filename=$2
new_backup_name_prefix=$3
new_backup_name="${new_backup_name_prefix}--backup"
openstack volume backup create $existing_volume_id --name $new_backup_name -f json > $new_backup_detail_filename
