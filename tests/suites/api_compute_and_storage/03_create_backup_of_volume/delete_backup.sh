#!/bin/bash
new_backup_detail_filename=$1
backup_id=$(jq -r '.id' $new_backup_detail_filename)
openstack volume backup delete $backup_id
