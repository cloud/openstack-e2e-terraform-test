variable "test_specific__infra_name" {
  type = string
}

resource "openstack_blockstorage_volume_v3" "source_volume" {
  name = "${var.test_specific__infra_name}--source_volume"
  size = 1
  description = "This resource is created as part of automated end-to-end tests"
}

resource "null_resource" "new_backup" {
  
  provisioner "local-exec" {
    command = "/bin/bash create_backup_of_volume.sh ${openstack_blockstorage_volume_v3.source_volume.id} new_backup_detail.json ${var.test_specific__infra_name}"
    
  }

  provisioner "local-exec" {
    when    = destroy
    command = "/bin/bash delete_backup.sh new_backup_detail.json"
  }

}

data "local_file" "new_backup_detail" {
  filename = "new_backup_detail.json"
  depends_on = [null_resource.new_backup]  # Ensure that this runs after the command
}

output "new_backup_detail" {
  value = jsondecode(data.local_file.new_backup_detail.content)
}
