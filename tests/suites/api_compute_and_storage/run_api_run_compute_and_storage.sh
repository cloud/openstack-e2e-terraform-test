#!/bin/bash

THIS_SCRIPT_DIR="$(dirname "$(realpath "$0")")"
THIS_SCRIPT_DIR/../../support/scripts/engage_test.sh --test_run $THIS_SCRIPT_DIR/01_create_vm
$THIS_SCRIPT_DIR/../../support/scripts/engage_test.sh --tear_down $THIS_SCRIPT_DIR/01_create_vm

$THIS_SCRIPT_DIR/../../support/scripts/engage_test.sh --test_run $THIS_SCRIPT_DIR/02_volume_to_snapshot_to_volume
$THIS_SCRIPT_DIR/../../support/scripts/engage_test.sh --tear_down $THIS_SCRIPT_DIR/02_volume_to_snapshot_to_volume

$THIS_SCRIPT_DIR/../../support/scripts/engage_test.sh --test_run $THIS_SCRIPT_DIR/03_create_backup_of_volume
$THIS_SCRIPT_DIR/../../support/scripts/engage_test.sh --tear_down $THIS_SCRIPT_DIR/03_create_backup_of_volume
