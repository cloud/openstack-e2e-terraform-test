
variable "test_specific__infra_name" {
  type = string
}
resource "openstack_networking_secgroup_v2" "test_security_group" {
  name        = "${var.test_specific__infra_name}--test_security_group"
  description = "Security group allowing SSH and FTP access"
}

resource "openstack_networking_secgroup_rule_v2" "ssh_rule" {
  direction    = "ingress"
  ethertype    = "IPv4"
  protocol     = "tcp"
  port_range_min = 22
  port_range_max = 22
  security_group_id = openstack_networking_secgroup_v2.test_security_group.id
  remote_group_id = openstack_networking_secgroup_v2.test_security_group.id
}

resource "openstack_networking_secgroup_rule_v2" "ftp_rule" {
  direction    = "ingress"
  ethertype    = "IPv4"
  protocol     = "tcp"
  port_range_min = 21
  port_range_max = 21
  security_group_id = openstack_networking_secgroup_v2.test_security_group.id
  remote_group_id = openstack_networking_secgroup_v2.test_security_group.id
}  


output "test_security_group_id" {
  value = openstack_networking_secgroup_v2.test_security_group.id
}
