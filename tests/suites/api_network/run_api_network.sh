#!/bin/bash
set -e

THIS_SCRIPT_DIR="$(dirname "$(realpath "$0")")"
$THIS_SCRIPT_DIR/../../support/scripts/engage_test.sh --test_run $THIS_SCRIPT_DIR/01_create_security_group
$THIS_SCRIPT_DIR/../../support/scripts/engage_test.sh --tear_down $THIS_SCRIPT_DIR/01_create_security_group

$THIS_SCRIPT_DIR/../../support/scripts/engage_test.sh --test_run $THIS_SCRIPT_DIR/02_create_router_network_subnet
$THIS_SCRIPT_DIR/../../support/scripts/engage_test.sh --tear_down $THIS_SCRIPT_DIR/02_create_router_network_subnet
