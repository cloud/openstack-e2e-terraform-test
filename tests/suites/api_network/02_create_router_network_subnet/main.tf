variable "project_specific__public_external_network_name" {}

variable "test_specific__infra_name" {
  type = string
}

resource "openstack_networking_network_v2" "test_network" {
  name           = "${var.test_specific__infra_name}--test_network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "test_subnet" {
  name           = "${var.test_specific__infra_name}--test_subnet"
  network_id      = openstack_networking_network_v2.test_network.id
  cidr            = "192.168.10.0/24" # Adjust the CIDR block as needed
  ip_version      = 4
  dns_nameservers = ["8.8.8.8"]
}

resource "openstack_networking_router_v2" "test_router" {
  name           = "${var.test_specific__infra_name}--test_router"
  admin_state_up = "true"
  external_network_id = data.openstack_networking_network_v2.external_network.id
}

resource "openstack_networking_router_interface_v2" "test_router_interface" {
  router_id = openstack_networking_router_v2.test_router.id
  subnet_id = openstack_networking_subnet_v2.test_subnet.id
}

data "openstack_networking_network_v2" "external_network" {
  name = var.project_specific__public_external_network_name
}
