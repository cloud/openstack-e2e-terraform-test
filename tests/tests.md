TF run
* create instance
  - expected state:
    - TF return code == 0
    - openstack server info: server status == running
  - tear down
    - TF destroy


- TF creates instance
  - rather small flavor (m1.medium)
  - rather large flavor (c3.16core-30ram)


network 06e6b8f4-a8fe-47b5-9c10-b7e0087bdf0d


```
openstack server create \
--image ubuntu-jammy-x86_64 \
--flavor e1.medium \
--key-name jkrystof_cloud \
--network group-project-network \
--wait \
jkrystof-delme
```


# dokumentace
- pro pristup ke strojum vytvorenych v ramci testu slouzi SSH klic ulozeny v synthetic_monitoring/tests/ansible/credentials/ssh_key_ostack.yml
  - TODO: dodat vault password
- servisni ukony - naplanovat purge projektu

### SSH keys - key-pairs for ostack

saved in vault (vault-synthetic-monitoring)
```
/home/jank/devel/git_repos/synthetic_monitoring/tests/ansible/credentials/vaul.yml
```

```
mkdir -p ~/.ssh/synthetic-monitoring/g2b
ssh-keygen -t ed25519 -C synthetic-monitoring@g2b -f ~/.ssh/synthetic-monitoring/g2b/id_ed25519 

mkdir -p ~/.ssh/synthetic-monitoring/g2o
ssh-keygen -t ed25519 -C synthetic-monitoring@g2b -f ~/.ssh/synthetic-monitoring/g2o/id_ed25519
```

BACKUP_PREFIX=synmon_test
VOL_ID=8b9b747e-da4b-4ecd-b94c-89019e5920e0
VOL_NAME=$(openstack volume show $VOL_ID -f json | jq -r '.name')
SNAPSHOT_NAME="${BACKUP_PREFIX}-${VOL_NAME}"
SNAPSHOT_ID=$(openstack volume snapshot create --volume $VOL_ID --force -f json $SNAPSHOT_NAME | jq -r '.id')

NEW_VOLUME_NAME="new__${VOL_NAME}"
NEW_VOLUME_ID=$(openstack volume create --snapshot $SNAPSHOT_ID $NEW_VOLUME_NAME -f json | jq -r '.id')


BACKUP_ID=$(openstack volume backup create --force --name backup_${VOL_NAME} $VOL_ID  -f json | jq -r '.id')

openstack volume backup delete $BACKUP_ID
openstack volume delete $NEW_VOLUME_ID
openstack volume snapshot delete $SNAPSHOT_ID




### schema
1 TF - vytvor VM s data diskem
  - ER: RC == 0
  - ER: existuje(ID_VOL_0)
2 spust bash - * snapshot(ID_VOL) 
  - ER: RC == 0 
  - ER: existuje(ID_S) 
3 spust bash - * volume(ID_S)
    - ER: RC == 0
    - ER: existuje(ID_VOL_S)
4 spust bash - * backup(ID_VOL_S) 
