#!/bin/bash

function print_help() {
  echo "HELP"  
  echo "****"
  echo "The script looks for resources in current openstack project and fails if there unexpected resources."
  echo "It returns rc == 0 if no resources are found or rc == 123 if any resource exists."
  echo "****"  
  echo ""  
  echo "Optional parameters:"    
  echo -e "\t--help\t\t\t- Print help and quits.\n"
  echo "Help:"
  return 0
}

while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      print_help
      exit 0
      ;;
    *)
      echo "Unknown option $1"
      print_help
      exit 2
      ;;
  esac
done

count_lines_in_string() {
  local input_string="$1"    
  if [[ -z "$input_string" ]]; then      
    echo 0
  else      
    printf "%s" "$X" | grep -c "^"
  fi
}


function report_if_count_not_matching_expected() {
  local sub_command="$1"
  local expected_count="$2"  
  local let current_count=0
  
  # run ostack query
  local response_json=$(openstack $sub_command -f json)
  local current_count=$(echo "$response_json" | jq length)
  number_of_unexpected_resources=$(($current_count - $expected_count))
  if [[ $number_of_unexpected_resources -gt 0 ]]; then
    # output formatting ... 
    single_line=$(echo $response_json | sed ':a;N;$!ba;s/\n/ /g')
    echo "problem: $sub_command reported ($current_count) resources while there were expected ($expected_count) resources. json_detail: $single_line"    
    return $number_of_unexpected_resources
  elif [[ $number_of_unexpected_resources -lt 0 ]]; then
    echo "warning: $sub_command reported less resources ($current_count) than expected ($expected_count): it means that some fundamental resources may be missing.  json_detail: $single_line"
    # zero is returned here intentionally as this warning does not imply immediate issue.... 
  fi

  return 0
}

function determine_existing_ostack_resources() {    
  local COMMAND_AND_EXPECTED_FUNDAMENTALRESOURCES_COUNT=(
    "server list:0"
    "volume list:0"
    "volume snapshot list:0"
    "volume backup list:0"
    "floating ip list:0"
    "router list:1" # group-project-router
    "network list:7" #group-project-network, internal-ipv4-general-private, external-ipv4-general-muni, external-ipv4-cerit-fi-public-147_251_88_128, external-ipv6-general-public, external-ipv4-general-public, external-ipv4-cerit-fi-public-147_251_91_192
    "subnet list:3" # group-project-network-subnet, external-ipv6-general-public-2001-718-801-43b, internal-ipv4-general-private-172-16-0-0 
    "port list:4" # # dvr 1x, dhcp 2x, snat 1x
  )
  local total_unexpected_resources_count=0 
  for record in "${COMMAND_AND_EXPECTED_FUNDAMENTALRESOURCES_COUNT[@]}" ; do
      command="${record%%:*}"
      expected_resources_count="${record##*:}"      
      report_if_count_not_matching_expected "$command" $expected_resources_count      
      let total_unexpected_resources_count=$total_unexpected_resources_count+$?      
  done     
  return $total_unexpected_resources_count
}



determine_existing_ostack_resources
unexpected_resources_count=$?
if [[ $unexpected_resources_count > 0 ]]; then
  echo "Openstack project contains unexpected resources of total count = $unexpected_resources_count" >&2
  exit 123
fi


echo "Openstack project does NOT contain any unexpected resources"
