function terraform_apply() {
  tf_directory="$1"  
  shift
  parameters="$@"
  cd $tf_directory
  terraform init
  terraform apply -auto-approve $parameters -var-file $tf_directory/../../conf/test_specific.tfvars -var-file $tf_directory/../../conf/project_specific.tfvars  
}

function terraform_destroy() {  
  tf_directory="$1"  
  shift
  parameters="$@"
  cd $tf_directory 
  terraform init
  terraform destroy -auto-approve $parameters -var-file $tf_directory/../../conf/test_specific.tfvars -var-file $tf_directory/../../conf/project_specific.tfvars
}


function run_tf_command() {
  command="$1"
  shift
  eval "$command $@"
  if [ $? -ne 0 ]; then
    echo "Terraform failed"
    exit 1
  fi
}

function terraform_output() {
  tf_directory="$1"
  output_name="$2"
  cd $tf_directory  
  terraform output -json $output_name 
}
