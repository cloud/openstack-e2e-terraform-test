#!/bin/bash
set -e

THIS_SCRIPT_DIRX=$(dirname "$(realpath "$0")")
source $THIS_SCRIPT_DIRX/commons.sh


flag_run=
flag_tear_down=
test_case_folder_location=



function print_help() {
  echo "HELP"  
  echo "****"
  echo "The script triggers a test run or triggers a tear down"
  echo "****"  
  echo ""  
  echo "Mutually exclusive parameters:"  
  echo -e "\t--test_run <test_case_folder_location>\t\t- Run the tests without tear down.\n"
  echo -e "\t--tear_down <test_case_folder_location>\t\t- remove all resources created in the 'run' phase.\n"  

  echo "Help:"
  echo -e "\t--help\t\t\t- Print help and quits.\n"
  return 0
}


function check_flag() {
  local flag=$1
    if [[ -n $flag ]]; then
        echo "Error: --test_run and --tear_down are flags rather than valued based parameters."  
        print_help
    fi        
}


while [[ $# -gt 0 ]]; do
  case $1 in
    --test_run)
      test_case_folder_location="$2"
      flag_run=true
      shift # past argument
      shift # past value
      ;;
    --tear_down)
      test_case_folder_location="$2"
      flag_tear_down=true
      shift # past argument
      shift # past value
      ;;
    -h|--help)      
      print_help
      exit 0
      ;;
    *)
      echo "Unknown option $1"
      print_help
      exit 2
      ;;
  esac
done


if [[ $flag_run && $flag_tear_down ]]; then
    echo "Error: parameters --test_run and --tear_down are mutually exclusive! Provide single option only!"
    print_help
    exit 1
fi


if ! [[ $flag_run || $flag_tear_down ]]; then
    echo "Error: either --test_run or --tear_down is mandatory!"
    print_help
    exit 2
fi


if [[ $flag_run ]]; then
  echo "running test $test_case_folder_location ..."
  run_tf_command terraform_apply $test_case_folder_location
  exit 0  
fi


if [[ $flag_tear_down ]]; then
  echo "running tear down for $test_case_folder_location ..."
  run_tf_command terraform_destroy $test_case_folder_location  
  exit 0  
fi
