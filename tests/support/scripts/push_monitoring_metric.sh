#!/bin/bash
set -e

function print_help() {
  echo "HELP"  
  echo "****"
  echo "The script reports successful execution of either 'test run' (default) or 'tear down' (provide flag --FLAG_TEAR_DOWN)."
  echo "It returns rc == 0 if report to the monitoring push gateway is successful, or rc > 0 in case of failure."  
  echo "****"  
  echo ""  
  echo "Mandatory parameters:"
  echo -e "\t--suite_name ...\t\t\t- Name of the suite.\n"    
  echo -e "\t--test_name ...\t\t\t- Name of the test.\n"    
  echo -e "\t--environment ...\t\t\t- Name of the environment (prod-brno, stage-ostrava, etc.)\n"    
  echo -e "\t--elapsed_time ...\t\t\t- Elapsed time in seconds.\n"    
  echo -e "\t--pushgateway_url ...\t\t\t- URL of the pushgateway. The URL does not include any additional path.\n"        
  echo -e "\t--pushgateway_credentials ...\t\t\t- The pushgateway credentials is a pair of username and password delimited by a semicolon, i.e. 'uname:pswd'.\n"          
  echo "Optional parameters:"  
  echo -e "\t--tear_down\t\t\t-flag: will report 'tear down' phase.\n"
  echo -e "\t--dry_run\t\t\t-flag: will not to the pushgateway\n"
  echo -e "\t--debug\t\t\t-flag: will report 'tear down' phase.\n"
  echo -e "\t--help\t\t\t-flag: Print help and quits.\n"
  echo "Help:"
  return 0
}


function check_mandatory_parameters() {
  local undefined_variables=""  
  for VARIABLE_NAME in $@; do    
    if [[ -z ${!VARIABLE_NAME} ]]; then            
      undefined_variables="$VARIABLE_NAME $undefined_variables"      
    else
      # if debug print out except of credentials
      [[ $FLAG_DEBUG && ! $(echo $VARIABLE_NAME | grep -i credential) ]] && echo "debug: $VARIABLE_NAME == ${!VARIABLE_NAME}"        
    fi
      
  done
  
  if [[ ${#undefined_variables} -gt 0 ]]; then
    echo "incorrect input - following variables are not defined: $undefined_variables"
    print_help
    exit 1 
  fi
  
}

while [[ $# -gt 0 ]]; do
  case $1 in
    --suite_name)
      SUITE_NAME="$2"
      shift # past argument
      shift # past value
      ;;
    --test_name)
      TEST_NAME="$2"
      shift # past argument
      shift # past value
      ;;
    --environment)
      ENVIRONMENT="$2"
      shift # past argument
      shift # past value
      ;;
    --elapsed_time)
      ELAPSED_TIME="$2"
      shift # past argument
      shift # past value
      ;;
    --pushgateway_url)
      PUSHGATEWAY_URL="$2"
      shift # past argument
      shift # past value
      ;;
    --pushgateway_credentials)
      PUSHGATEWAY_CREDENTIALS="$2"
      shift # past argument
      shift # past value
      ;;    
    --tear_down)
      FLAG_TEAR_DOWN=true
      shift # past argument
      ;;
    --debug)
      FLAG_DEBUG=true
      shift # past argument
      ;;
    --dry_run)
      FLAG_DRY_RUN=true
      shift # past argument
      ;;
    -h|--help)      
      print_help
      exit 0
      ;;
    *)
      echo "Unknown option $1"
      print_help
      exit 2
      ;;
  esac
done


check_mandatory_parameters SUITE_NAME TEST_NAME ENVIRONMENT PUSHGATEWAY_URL ELAPSED_TIME PUSHGATEWAY_CREDENTIALS
# additional adjustments
PHASE=$([ $FLAG_TEAR_DOWN ] && echo "tear_down" || echo "test_run")
PUSHGATEWAY_URL="$PUSHGATEWAY_URL/metrics/job/e2e"

# prepare the metric representation the prometheus 
METRIC="e2e_test{app=\"e2e\", environment=\"$ENVIRONMENT\", suite=\"$SUITE_NAME\", test=\"$TEST_NAME\", phase=\"$PHASE\"} $ELAPSED_TIME"
[[ $FLAG_DEBUG ]] && echo "debug: metric $METRIC"

if [[ $FLAG_DRY_RUN ]]; then
  echo "quitting without pushing as it is a dry-run"
  exit 0
fi

cat <<EOF | curl -v --user "$PUSHGATEWAY_CREDENTIALS" --data-binary @- $PUSHGATEWAY_URL
  $METRIC
EOF
