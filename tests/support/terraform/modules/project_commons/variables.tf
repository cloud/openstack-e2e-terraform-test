variable "environment" {
  type = string
}

variable "default_ssh_user" {
  type = string
}
