variable "infra_name" {
  type = string
}

variable "node_count" {
  type = string
  default = 1
}

variable "node_image_name" {
  type = string
  default = "ubuntu-jammy-x86_64"
}

data "openstack_images_image_v2" "node_image" {
  name = var.node_image_name
}

variable "node_flavor_name" {
  type = string
}

variable "root_disk_size" {
  type = number
  default = 20
}

variable "data_disk_size" {
  type = number
  default = 30
}

variable "enable_public_fip" {
  description = "Flag to determine whether to allocate floating IPs."
  type = bool
  default = false
}

variable "subnet_name" {
  type = string
}

data "openstack_networking_subnet_v2" "subnet" {
  name = var.subnet_name
}

variable "project_network_id" {
  type = string
}

variable "security_group_ssh_id" {
  type = string
}

variable "public_external_network_name" {
  type = string
}

data "openstack_networking_network_v2" "project_network" {
  network_id = var.project_network_id
}

data "openstack_networking_network_v2" "public_external_network" {
  name = var.public_external_network_name
}

data "openstack_networking_secgroup_v2" "sec_group_ssh" {
  secgroup_id = var.security_group_ssh_id
}
