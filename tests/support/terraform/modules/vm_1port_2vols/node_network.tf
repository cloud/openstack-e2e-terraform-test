resource "openstack_networking_floatingip_associate_v2" "node_fip_associate" {
  count = var.enable_public_fip ? var.node_count : 0

  floating_ip = openstack_networking_floatingip_v2.node_fip[count.index].address
  port_id = openstack_networking_port_v2.node_port[count.index].id
}

resource "openstack_networking_port_v2" "node_port" {
  count = var.node_count

  name = "${var.infra_name}-node-port"
  network_id = data.openstack_networking_network_v2.project_network.id
  admin_state_up = "true"
  security_group_ids = [ data.openstack_networking_secgroup_v2.sec_group_ssh.id ]
  fixed_ip {
    subnet_id = data.openstack_networking_subnet_v2.subnet.id
  }
}

resource "openstack_networking_floatingip_v2" "node_fip" {
  count = var.enable_public_fip ? var.node_count : 0
  pool = data.openstack_networking_network_v2.public_external_network.name
}
