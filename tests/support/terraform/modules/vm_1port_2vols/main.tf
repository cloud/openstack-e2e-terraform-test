variable "environment" {
  type = string
}

variable "default_ssh_user" {
  type = string
}

variable "description" {
  default = ""
}

module "project_commons" {
  source = "../project_commons"
  environment = var.environment
  default_ssh_user = var.default_ssh_user
}

output "cloud_init_rendered" {
  value = module.project_commons.cloud_init_rendered
}
